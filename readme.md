# Task API pour le test technique Tilkee

![test-coverage](docs/coverage.svg)

## Quickstart

L'API est déployée à l'adresse [https://tilkee-test.herokuapp.com](https://tilkee-test.herokuapp.com), avec un [OpenAPI](https://tilkee-test.herokuapp.com/swagger-ui).

### Tester l'application en ligne

#### En utilisant les exemples fournis avec l'application

```
git clone https://gitlab.com/matngo/tilkee-test
cd tilkee-test
pip install requests
python3 client_examples/create_task.py
python3 client_examples/get_results.py
python3 client examples/revoke_task.py
```

### Deployer en local

```
git clone https://gitlab.com/matngo/tilkee-test
cd tilkee-test
docker-compose up
```

L'API devient disponible à l'adresse `http://localhost:8000/`.

On peut relancer les mêmes tests qu'auparavant en changeant le fichier `client_examples/conf.py`, en modifiant la ligne

```python
# client_examples/conf.py
# base_url = "https://tilkee-test.herokuapp.com/"
base_url = "http://localhost:8000/"
```

### Lancer les tests en local

```
pip install ."[test]"
```

`./scripts/run_tests.sh` permet de lancer les tests dans une version containerisée de l'application.

## Stack technique

### Description

L'application est une API Flask, avec un worker Celery qui repose sur un broker Redis. L'application et le worker font tous deux persister les données dans une base de données postgreSQL.
L'API Flask utilise l'ORM SQLAlchemy.

Les choix techniques ont été faits avec en tête :

- la performance de la solution,
- la maîtrise personnelle de la stack (i.e. des solutions industrialisées, documentées et déjà éprouvées personnellement),
- la vitesse de développement.

### Architecture logicielle

J'avais pour objectif que cette application soit le plus industrialisable possible, au regard des attendus exposés dans le cahier des charges.
Afin de rendre l'application la plus évolutive possible, il est donc fourni un wrapper aux tâches, afin de les intégrer les plus facilement possible dans l'api.
Pour créer une nouvelle tâche (i.e. un nouveau endpoint asynchrone), il suffit donc de :

- implémenter une classe dérivée de TaskABC, en implémentant les méthodes `eta` et `_run`, ainsi qu'ne précisant le schéma d'entrée de la donnée ; qui permettra une validation des paramètres passées en HTTP. Les fonctions `eta` et `_run` utilisent les paramètres définis par ce schéma en keyword args.

```python
# task_api/core/new_task.py
"""
A short running task
"""

from marshmallow import fields

from .task_abc import TaskABC


class NewTask(TaskABC):
    """
    A new task implementation
    """

    request_schema = {"arg_1": fields.String(), "arg_2": fields.Int()}

    def eta(self, arg_1: str = "", arg_2:int = 0 **kwargs):
        return 0

    def _run(*args, arg_1: str = "", arg_2: int = 0, **kwargs):
        return {arg_1: arg_2}
```

- déclarer la tâche comme une vue, en utilisant le wrapper fourni dans `task_api/tasks/views.py`

```python
# task_api/tasks/views.py
# ... import
from task_api.core.new_task import NewTask

# ...
register_tasks(NewTask)
```

## Améliorations à prévoir

- [] Les contraintes de clés étrangères entre la table Task gérée par l'API et la table TaskMeta gérée par Celery.
- [] Implémenter une CI
- [] Augmenter la couverture de tests
