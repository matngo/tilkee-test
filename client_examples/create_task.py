from pprint import pprint

import requests
from conf import admin, long_task_url, short_task_url, user1, user2


def create_long_tasks(n: int, user, time_to_sleep=10):
    resps = [
        requests.post(
            long_task_url,
            json={"time_to_sleep": time_to_sleep},
            headers=user,
        )
        for _ in range(n)
    ]
    pprint([resp.json() for resp in resps])

def create_short_tasks(n: int, user):
    resps = [
        requests.post(
            short_task_url,
            json={"message": "short-task"},
            headers=user,
        )
        for _ in range(n)
    ]
    pprint([resp.json() for resp in resps])



pprint("-------- CREATE TASKS ----------")

pprint("-------- SHORT_TASKS----------")
create_short_tasks(3, admin)
create_short_tasks(2, user1)
create_short_tasks(2, user2)

pprint("-------- LONG_TASKS----------")
create_long_tasks(4, admin)
create_long_tasks(2, user1)
create_long_tasks(2, user2)
