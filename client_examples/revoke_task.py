import time
from pprint import pprint

import requests
from conf import admin, base_url, long_task_url, user1

pprint("--------  ADMIN REVOKE TASK ----------")
result_path = requests.post(
    long_task_url,
    json={"time_to_sleep": 1000},
    headers=user1,
).json()["result_path"]

requests.delete(
    base_url + result_path,
    headers=admin,
)
time.sleep(0.1)
revoke_result = requests.get(base_url + result_path, headers=user1).json()
pprint(revoke_result)
