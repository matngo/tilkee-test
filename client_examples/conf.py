import requests

base_url = "https://tilkee-test.herokuapp.com/"
# base_url = "http://localhost:5000/"
login_url = base_url + "login"

admin_token = (
    requests.post(login_url, json={"username": "admin", "password": "admin"})
    .json()
    .get("authToken")
)

user1_token = (
    requests.post(login_url, json={"username": "user_1", "password": "user_1"})
    .json()
    .get("authToken")
)

user2_token = (
    requests.post(login_url, json={"username": "user_2", "password": "user_2"})
    .json()
    .get("authToken")
)

admin = {"authorization": admin_token}
user1 = {"authorization": user1_token}
user2 = {"authorization": user2_token}
users_url = base_url + "users/"
tasks_url = base_url + "tasks/"
short_task_url = tasks_url + "short-task/"
long_task_url = tasks_url + "long-task/"
