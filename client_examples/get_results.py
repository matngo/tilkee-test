from pprint import pprint

import requests
from conf import admin, long_task_url, short_task_url, user1, user2, users_url


def sample_results(user):
    short_results = requests.get(short_task_url, headers=user).json()
    pprint("-------- SHORT TASK----------")
    pprint(
        {
            "num_tasks": len(short_results),
            "example": short_results[0],
            "users": set([t["username"] for t in short_results]),
        }
    )
    pprint("-------- LONG TASK ----------")
    long_results = requests.get(long_task_url, headers=user).json()
    pprint(
        {
            "num_tasks": len(long_results),
            "example": long_results[0],
            "users": set([t["username"] for t in long_results]),
        }
    )


pprint("-------- GET RESULTS ----------")
pprint("---------- ADMIN ------------")
sample_results(admin)


pprint("---------- USER_1 ------------")
sample_results(user1)

pprint("---------- USER_2 ------------")
sample_results(user2)

pprint("-------- ADMIN DASHBOARD ----------")
pprint(
    requests.get(users_url, headers=admin).json(),
)
pprint(
    requests.get(users_url + "/1", headers=admin).json(),
)
pprint("not logged in :")
pprint(requests.get(users_url, headers=user1).status_code)
