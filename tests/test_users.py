import time

from task_api.tasks.models import Task, TaskMeta


def test_users_view(client, user_auth, admin_auth):
    client.post("/tasks/long-task/", json={"time_to_sleep": 0}, headers=user_auth)
    client.post("/tasks/long-task/", json={"time_to_sleep": 0}, headers=admin_auth)
    client.post("/tasks/short-task/", json={}, headers=admin_auth)
    resp = client.get("/users/", headers=admin_auth).json
    assert resp["admin"]["long-task"] == 1
    assert resp["admin"]["short-task"] == 1
    assert resp["user"]["long-task"] == 1


def test_user_view(client, user, user_auth, admin, admin_auth):
    client.post("/tasks/long-task/", json={"time_to_sleep": 0}, headers=user_auth)
    client.post("/tasks/long-task/", json={"time_to_sleep": 0}, headers=admin_auth)
    client.post("/tasks/short-task/", json={}, headers=admin_auth)
    resp = client.get(f"/users/{user.id}", headers=admin_auth).json
    assert resp["long-task"] == 1
    resp = client.get(f"/users/{admin.id}", headers=admin_auth).json
    assert resp["short-task"] == 1
    assert resp["long-task"] == 1
