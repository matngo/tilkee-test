import time

from task_api.tasks.models import Task, TaskMeta

LONG_TIME = 0.2
SHORT_TIME = 0


def test_long_task(client, user_auth):
    resp = client.post(
        "/tasks/long-task/", json={"time_to_sleep": LONG_TIME}, headers=user_auth
    )
    assert resp.status_code == 201
    assert "result_path" in resp.json
    result_path = resp.json.get("result_path")
    time.sleep(LONG_TIME + 0.1)
    result = client.get(result_path, headers=user_auth).json
    r = result.get("result")
    assert r.get("after_sleep") == LONG_TIME
    assert r.get("greetings") == "hello"
    assert r.get("to") == "world"


def test_wrong_args(client, user_auth):
    resp = client.post("/tasks/long-task/", json={"wrong_args": 1}, headers=user_auth)
    assert resp.status_code == 422


def test_revoke_task(client, admin_auth):
    resp = client.post(
        "/tasks/long-task/", json={"time_to_sleep": LONG_TIME}, headers=admin_auth
    )
    result_path = resp.json.get("result_path")
    resp = client.delete(result_path, headers=admin_auth)
    assert resp.status_code == 204
    time.sleep(0.1)
    result = client.get(result_path, headers=admin_auth)
    assert result.json.get("status") == "REVOKED"
    assert Task.query.count() == 1
    assert TaskMeta.query.count() == 1


def test_delete_finished_task(client, admin_auth):
    resp = client.post(
        "/tasks/long-task/", json={"time_to_sleep": LONG_TIME}, headers=admin_auth
    )
    result_path = resp.json.get("result_path")
    time.sleep(LONG_TIME + 0.1)
    resp = client.delete(result_path, headers=admin_auth)
    assert resp.status_code == 204
    assert Task.query.count() == 0
    assert TaskMeta.query.count() == 0


def test_revoke_task_as_user(client, user_auth):
    resp = client.post(
        "/tasks/long-task/", json={"time_to_sleep": LONG_TIME}, headers=user_auth
    )
    result_path = resp.json.get("result_path")
    resp = client.delete(result_path, headers=user_auth)
    assert resp.status_code == 403
