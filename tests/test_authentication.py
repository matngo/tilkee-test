import jwt

from task_api.users.model import User


def test_wrong_password(app, client, user):
    resp = client.post("/login", json=dict(username="user", password="wrong"))
    assert resp.status_code == 403


def test_wrong_username(app, client, user):
    resp = client.post("/login", json=dict(username="wrong", password="user"))
    assert resp.status_code == 403


def test_success_login(app, client, user):
    resp = client.post("/login", json=dict(username="user", password="user"))
    assert resp.status_code == 200
    assert "authToken" in resp.json
    token = resp.json.get("authToken")
    data = jwt.decode(
        bytes(token, "utf-8"),
        app.secret_key,
        algorithms=[app.config.get("JWT_ALGORITHM")],
    )
    assert data.get("username") == "user"
    assert not data.get("admin")
