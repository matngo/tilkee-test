import pytest

from task_api.app import create_app
from task_api.database import db
from task_api.users.model import User


@pytest.fixture(scope="function")
def app():
    app = create_app()
    with app.app_context():
        db.create_all()
        yield app
        db.drop_all()


@pytest.fixture(scope="function")
def client(app):
    yield app.test_client()


@pytest.fixture(scope="function")
def admin(app):
    admin = User(username="admin", password="admin", admin=True)
    db.session.add(admin)
    db.session.flush()
    yield admin
    db.session.rollback()


@pytest.fixture(scope="function")
def user(app):
    user = User(username="user", password="user", admin=False)
    db.session.add(user)
    db.session.flush()
    yield user
    db.session.rollback()


@pytest.fixture(scope="function")
def user_token(client, user):
    resp = client.post("/login", json=dict(username="user", password="user"))
    return resp.json.get("authToken")


@pytest.fixture(scope="function")
def user_auth(user_token):
    return {"authorization": user_token}


@pytest.fixture(scope="function")
def admin_token(client, admin):
    resp = client.post("/login", json=dict(username="admin", password="admin"))
    return resp.json.get("authToken")


@pytest.fixture(scope="function")
def admin_auth(admin_token):
    return {"authorization": admin_token}
