def test_not_logged_in_get_tasks(client):
    resp = client.get("/users/")
    assert resp.status_code == 403


def test_logged_in_as_admin(client, admin_token):
    resp = client.get("/users/", headers={"authorization": admin_token})
    assert resp.status_code == 200
