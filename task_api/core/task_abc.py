"""
Abstract wrapper around a core function.
"""

from abc import ABC, abstractmethod
from typing import Dict

from flask import current_app as app
from marshmallow import Schema

from task_api.app import celery
from task_api.tasks.models import Task
from functools import wraps


class TaskABC(ABC):
    """
    A Task is a wrapper around a core function.
    To implement a task, one should implement an ETA method,
    estimating the length of the run, and the actual run method.

    If the eta is greather than a threshold, then the result is not awaited for.
    This threshold can be overwritten by the Task API.
    """

    ASYNC_THRESHOLD: int = 5
    request_schema: Schema

    def __init__(self, task_name="test"):
        self._async_run = celery.task(track_started=True)(self._run)

    @abstractmethod
    def eta(self, **kwargs):
        """
        Estimates the time taken by the task given the payload
        """
        raise NotImplementedError

    @abstractmethod
    def _run(*args, **kwargs):
        """
        Implementation of the core function
        """
        raise NotImplementedError

    def run(self, **kwargs) -> Dict:
        """
        Runs the Celery-wrapped main method.
        If the eta is greater than the threshold,
        the wrapper sends a received message with the task id.
        If not, it waits for the result, then send the result directly.
        """
        eta = self.eta(**kwargs)
        if eta > app.config.get("ASYNC_THRESHOLD", self.ASYNC_THRESHOLD):
            task = self._async_run.delay(**kwargs)
            return {"status": 201, "task_id": task.id}
        else:
            task = self._async_run.delay(**kwargs)
            return {"status": 200, "data": self._run(**kwargs), "task_id": task.id}
