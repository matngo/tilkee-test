"""
This module contains all the 'core' functions.
Those functions are wrapped within an Task abstraction,
which converts a function into a Celery Task.
"""
