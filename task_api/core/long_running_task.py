"""
A long running task
"""

import time
from typing import Optional

from marshmallow import Schema, fields

from .task_abc import TaskABC


class RequestSchema(Schema):
    time_to_sleep = fields.Float()


class LongTask(TaskABC):
    """
    Long task implementation
    """

    request_schema = RequestSchema()

    def eta(self, time_to_sleep: Optional[float] = 0, **kwargs):
        """
        The run time is the time to sleep
        """
        if time_to_sleep is None:
            return 10
        else:
            return time_to_sleep

    def _run(*args, time_to_sleep: Optional[float] = 0, **kwargs):
        """
        Waits for some time then says hello back.
        """
        time.sleep(time_to_sleep)
        return {"greetings": "hello", "to": "world", "after_sleep": time_to_sleep}
