"""
A short running task
"""

from marshmallow import fields

from .task_abc import TaskABC


class ShortTask(TaskABC):
    """
    Implementation of the short task
    """

    request_schema = {"message": fields.String()}

    def eta(self, message: str = "", **kwargs):
        """
        This function runs instantly
        """
        return 0

    def _run(*args, message: str = "", **kwargs):
        """
        Returns the original message and a reversed message
        """
        return {"original_message": message, "reversed_message": message[::-1]}
