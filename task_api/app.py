"""
Main app module
"""

import os

from celery import Celery
from flask import Flask
from flask_apispec import FlaskApiSpec

from .config import config_map
from .database import db, migrate

environment = os.environ.get("environment", "debug")
config = config_map.get(environment)
docs = FlaskApiSpec()

# Creates a celery worker with the app config
celery = Celery(
    __name__,
    backend=config.CELERY_BACKEND_URL,
    broker=config.CELERY_BROKER_URL,
)


def create_app():
    """
    Flask app factory pattern
    """
    app = Flask(__name__)
    app.config.from_object(config)

    from .tasks import models
    from .users import model

    setup_blueprints(app)
    setup_plugins(app)
    return app


def setup_plugins(app: Flask):
    """
    Setups the plugins used by the app
    """
    db.init_app(app)
    migrate.init_app(app, db=db)
    docs.init_app(app)


def setup_blueprints(app: Flask):
    """
    Registers the blueprints of the app.
    """
    from .auth import auth_api
    from .tasks.views import task_api
    from .users.views import users_api

    app.register_blueprint(auth_api)
    app.register_blueprint(task_api)
    app.register_blueprint(users_api)
