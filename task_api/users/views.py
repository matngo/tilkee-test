"""
A user view, to see all tasks run by a selected user
"""

from collections import defaultdict

from flask import Blueprint
from flask_apispec import MethodResource, doc
from sqlalchemy import func

from task_api.app import docs
from task_api.auth import login_required
from task_api.database import db
from task_api.tasks.models import Task
from task_api.users.model import User

users_api = Blueprint("users", "users", url_prefix="/users/")


@doc(
    description="dashboard for admin",
    tags=["users"],
    params={
        "Authorization": {
            "description": (
                "Authorization HTTP header with JWT admin access token"
                "like: authorization: asdf.qwer.zxcv"
            ),
            "in": "header",
            "type": "string",
            "required": True,
        }
    },
)
class UserView(MethodResource):
    """
    The user view allows the admin to have stats about
    the tasks run by each user.
    """

    @login_required(needs_admin=True)
    def get(self, user_id=None):
        if user_id is None:
            result = defaultdict(dict)
            user_task_type_count = (
                db.session.query(
                    User.username, Task.task_type, func.count(Task.task_type)
                )
                .filter(User.id == Task.user_id)
                .group_by(User.username, Task.task_type)
                .all()
            )
            for username, task_type, count in user_task_type_count:
                result[username][task_type] = count

            return dict(result), 200

        else:
            result = dict()
            task_type_count = (
                db.session.query(Task.task_type, func.count(Task.task_type))
                .filter(User.id == user_id)
                .filter(User.id == Task.user_id)
                .group_by(Task.task_type)
                .all()
            )
            for task_type, count in task_type_count:
                result[task_type] = count

            return result, 200


user_view = UserView.as_view("users")
users_api.add_url_rule(
    "/",
    view_func=user_view,
    methods=["GET"],
)

users_api.add_url_rule(
    "/<user_id>",
    view_func=user_view,
    methods=["GET"],
)

docs.register(UserView, blueprint="users", endpoint="users")
