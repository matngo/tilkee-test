"""
User model module
"""

from werkzeug.security import generate_password_hash

from task_api.common.mixins import CRUDMixin
from task_api.database import db


class User(db.Model, CRUDMixin):
    """
    User model
    """

    __table_args__ = {"extend_existing": True}
    __tablename__ = "users"

    username = db.Column(db.String(255), unique=True, nullable=False)
    password = db.Column(db.String(255), nullable=False)
    admin = db.Column(db.Boolean, nullable=False, default=False)

    def __init__(self, **kwargs):
        """
        Creates a user with a hashed + salt password
        """
        super().__init__(**kwargs)
        self.password = generate_password_hash(kwargs.get("password", ""))

    @classmethod
    def get_by_username(cls, username: str) -> "User":
        """
        Gets a user by username
        """
        return cls.query.filter_by(username=username).first() or None
