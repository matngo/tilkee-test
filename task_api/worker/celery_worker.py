"""
Celery worker
"""
from task_api.app import celery, create_app

app = create_app()


class ContextTask(celery.Task):
    """
    Middleware to push the app context to the Celery worker
    """

    def __call__(self, *args, **kwargs):
        with app.app_context():
            return self.run(*args, **kwargs)


celery.conf.update(app.config)
celery.Task = ContextTask
celery.conf.imports = celery.conf.imports + ("task_api.core",)
