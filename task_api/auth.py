"""
Auth module using JWT.
"""

from datetime import datetime, timedelta
from functools import wraps
from typing import Callable

import jwt
from flask import Blueprint, Response
from flask import current_app as app
from flask import request
from flask_apispec import doc, use_kwargs
from marshmallow import Schema, fields
from werkzeug.security import check_password_hash

from task_api.app import docs

from .users.model import User

auth_api = Blueprint(__name__, "auth")


class LoginSchema(Schema):
    """
    Login schema for the route
    """

    username = fields.String()
    password = fields.String()


@doc(tags=["login"])
@auth_api.route("/login", methods=["POST"])
@use_kwargs(LoginSchema())
def login(username=None, password=None):
    """
    Login route
    """
    if not username or not password:
        return Response(status=400)

    user = User.get_by_username(username)

    if not (user and check_password_hash(user.password, password)):
        return Response(status=403)

    token = jwt.encode(
        {
            "username": user.username,
            "is_admin": user.admin,
            "exp": datetime.utcnow() + timedelta(hours=1),
        },
        app.config.get("SECRET_KEY"),
        algorithm=app.config.get("JWT_ALGORITHM"),
    )
    return {"authToken": token.decode("utf-8")}, 200


def login_required(needs_admin: bool):
    """
    Decorator to protect a route.
    """

    def wrapper(endpoint: Callable):
        @wraps(endpoint)
        def wrapped(*args, **kwargs):
            token = request.headers.get("authorization")
            if not token:
                return {}, 403

            try:
                identity = jwt.decode(
                    bytes(token, encoding="utf-8"),
                    app.config.get("SECRET_KEY"),
                    algorithms=[app.config.get("JWT_ALGORITHM")],
                )

            except (
                jwt.ExpiredSignatureError,
                jwt.InvalidTokenError,
                jwt.ImmatureSignatureError,
            ):
                return {}, 403

            if needs_admin and not identity.get("is_admin"):
                return {}, 403

            return endpoint(*args, **kwargs)

        return wrapped

    return wrapper


def current_user():
    """
    Gets the logged in user.
    """
    token = request.headers.get("authorization")
    identity = jwt.decode(
        bytes(token, encoding="utf-8"),
        app.config.get("SECRET_KEY"),
        algorithms=[app.config.get("JWT_ALGORITHM")],
    )
    return User.get_by_username(identity.get("username"))


docs.register(login, blueprint=__name__)
