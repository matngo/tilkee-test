"""
A module to wrap core functions into Task views for the API
"""

from typing import List

from flask import Blueprint
from flask_apispec import MethodResource, doc, use_kwargs

from task_api.app import celery, docs
from task_api.auth import current_user, login_required
from task_api.common.helpers import camelcase_to_slug
from task_api.core.long_running_task import LongTask
from task_api.core.short_running_task import ShortTask
from task_api.core.task_abc import TaskABC

from .models import Task, TaskMeta

task_api = Blueprint("tasks", "tasks", url_prefix="/tasks/")


def register_tasks(*args: List[TaskABC]):
    """
    Register all tasks
    """
    for task in args:
        register_task(task)


def register_task(task_cls: TaskABC):
    """
    Wraps a task into a dynamically created resource.
    """

    @doc(
        tags=[camelcase_to_slug(task_cls.__name__)],
        description=task_cls._run.__doc__,
        params={
            "Authorization": {
                "description": (
                    "Authorization HTTP header with JWT admin access token"
                    "like: authorization: asdf.qwer.zxcv"
                ),
                "in": "header",
                "type": "string",
                "required": True,
            }
        },
    )
    class NewTaskHandler(MethodResource):
        """
        Wrapper class for our core functions
        """

        task = task_cls()
        task_route = camelcase_to_slug(task_cls.__name__)
        task_blueprint = Blueprint(task_route, task_route)

        def serialize_task(self, task: Task):
            """
            Serializer for the task result
            """
            meta = task.meta
            time_elapsed = None
            result = None
            date_done = None
            status = "PENDING"

            if meta:
                date_done = meta.date_done
                status = meta.status

                if status == "SUCCESS":
                    time_elapsed = (
                        task.meta.date_done - task.date_start
                    ).total_seconds()
                    result = self.task._async_run.AsyncResult(task.task_meta_id).get()

            return {
                "task_id": task.task_meta_id,
                "user_id": task.user_id,
                "username": task.user.username,
                "task_type": task.task_type,
                "status": status,
                "date_start": task.date_start,
                "date_done": date_done,
                "time_elapsed": time_elapsed,
                "result": result,
                "asynchronous": task.asynchronous,
            }

        @login_required(needs_admin=False)
        def get(self, task_id=None):
            """
            Route to get the result of a given task.
            If no id is given, returns all tasks for the logged in user.
            If the user is an admin, return all tasks.
            """
            user = current_user()
            if task_id is None:
                tasks = Task.query.filter_by(task_type=self.task_route)
                if not user.admin:
                    tasks = tasks.filter_by(user=user)

                return [self.serialize_task(task) for task in tasks.all()], 200
            else:
                task = Task.get_by_task_id(task_id)

                if user.id != task.user_id:
                    if not user.admin:
                        return {}, 403

                return self.serialize_task(task), 200

        @use_kwargs(task.request_schema)
        @login_required(needs_admin=False)
        def post(self, **kwargs):
            """
            Send the workload to the celery worker app.
            """
            user = current_user()
            resp = self.task.run(**kwargs)
            t = Task(
                task_meta_id=resp["task_id"],
                user_id=user.id,
                task_type=self.task_route,
            )
            if resp["status"] == 201:
                t.asynchronous = True
                t.save()
                return {
                    "result_path": f"/tasks/{NewTaskHandler.task_route}/{resp['task_id']}",
                    "task_id": resp["task_id"],
                }, 201

            elif resp["status"] == 200:
                t.asynchronous = True
                t.save()
                return resp["data"], 200

        @login_required(needs_admin=True)
        def delete(self, task_id):
            """
            If a task is pending, revokes it.
            If a task is finished (SUCESS or FAILURE), delete the results
            """
            task = Task.get_by_task_id(task_id)
            if meta := task.meta:
                status = meta.status
            else:
                status = "PENDING"
            if status in (
                "STARTED",
                "PENDING",
            ):
                celery.control.revoke(task_id, terminate=True)
            else:
                meta = task.meta
                Task.delete(task.id)
                TaskMeta.delete(meta.id)
            return {}, 204

    task_view = NewTaskHandler.as_view(NewTaskHandler.task_route)
    task_api.add_url_rule(
        f"/{NewTaskHandler.task_route}/",
        # defaults={"task_id": None},
        view_func=task_view,
        methods=["GET"],
    )

    task_api.add_url_rule(
        f"/{NewTaskHandler.task_route}/",
        view_func=task_view,
        methods=["POST"],
    )

    task_api.add_url_rule(
        f"/{NewTaskHandler.task_route}/<task_id>",
        view_func=task_view,
        methods=["GET", "DELETE"],
    )
    docs.register(NewTaskHandler, blueprint="tasks", endpoint=NewTaskHandler.task_route)

    return task_cls


register_tasks(LongTask, ShortTask)
