"""
Task and TaskMeta models.
"""

import time
from datetime import datetime

from sqlalchemy.dialects.postgresql import BYTEA

from task_api.common.mixins import CRUDMixin
from task_api.database import db


class TaskMeta(db.Model, CRUDMixin):
    """
    An representation of the TaskMeta from Celery.
    The model is provided directly by the celery worker.
    """

    __tablename__ = "celery_taskmeta"
    id = db.Column(
        db.Integer, db.Sequence("task_id_sequence"), primary_key=True, unique=True
    )
    task_id = db.Column(db.String(155))
    status = db.Column(db.String(50))
    date_done = db.Column(db.DateTime)
    traceback = db.Column(db.Text())
    name = db.Column(db.String(155))
    worker = db.Column(db.String(155))
    retries = db.Column(db.Integer)
    queue = db.Column(db.String(155))
    result = db.Column(BYTEA)
    args = db.Column(BYTEA)
    kwargs = db.Column(BYTEA)


class Task(db.Model, CRUDMixin):
    """
    We use this model to persist more data on the run tasks.
    It is populated each time a worker accepts a task.
    """

    __table_args__ = {"extend_existing": True}
    __tablename__ = "tasks"

    task_meta_id = db.Column(
        db.String(155),
        unique=True,
    )
    user_id = db.Column(db.Integer, db.ForeignKey("users.id"), nullable=False)
    user = db.relationship("User", backref="tasks", lazy=True)
    task_type = db.Column(db.String(255), nullable=False)
    date_start = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    asynchronous = db.Column(db.Boolean)

    @classmethod
    def get_by_task_id(cls, task_id):
        """
        Get a task by its meta id
        """
        return cls.query.filter_by(task_meta_id=task_id).first()

    @classmethod
    def get_by_type(cls, type):
        """
        Get a task by task type
        (i.e. each of the core functions defined in the task_api.core)
        """
        return [t.task_id for t in cls.query.filter_by(task_type=type)]

    @property
    def meta(self):
        return TaskMeta.query.filter_by(task_id=self.task_meta_id).first()
