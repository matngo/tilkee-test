"""
Helpers functions for the API
"""

import re


def camelcase_to_slug(s: str) -> str:
    """
    Converts a CamelCase string into a slug-like-string.
    """
    return re.sub(r"(?<!^)(?=[A-Z])", "-", s).lower()
