"""
Mixins for the database
"""

from sqlalchemy import Column, Integer

from task_api.database import db


class CRUDMixin(object):
    """
    Implements standard CRUD operations into the db.Model object
    """

    id = Column(Integer, primary_key=True)

    @classmethod
    def get_by_id(cls, _id: int):
        """
        Gets an object by id
        """
        return cls.query.get(_id)

    @classmethod
    def add(cls, **kwargs):
        """
        Add an object into the db
        """
        obj = cls(**kwargs)
        return obj.save()

    @classmethod
    def delete(cls, _id):
        """
        Delete an object by id
        """
        obj = cls.query.get(_id)
        db.session.delete(obj)
        db.session.commit()
        return {}

    @classmethod
    def update(cls, _id, **kwargs):
        """
        Update an existing object
        """
        obj = cls.query.filter_by(id=_id)
        obj.update(kwargs)
        db.session.commit()
        return obj

    def save(self):
        """
        Commits an object into the db
        """
        db.session.add(self)
        db.session.commit()
        return self
