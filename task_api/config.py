"""
Flask app configuration
"""

import os


class BaseConfig:
    """
    Base config from which they all inherits
    """

    SQLALCHEMY_DATABASE_URI = os.environ.get(
        "DATABASE_URL", "postgresql://tilkee:tilkee@localhost:5432/tasks"
    )
    CELERY_BACKEND_URL = "db+postgresql://tilkee:tilkee@localhost:5432/tasks"
    CELERY_BROKER_URL = "redis://localhost:6379/"
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SECRET_KEY = os.environ.get("SECRET_KEY", "secret")
    JWT_ALGORITHM = "HS512"
    ASYNC_THRESHOLD = 1
    task_serializer = "json"
    result_serializer = "json"
    task_track_started = True


class TestConfig(BaseConfig):
    """
    Test config used when testing on local docker stack
    """

    SQLALCHEMY_DATABASE_URI = "postgresql://postgres:postgres@localhost:6666/postgres"
    CELERY_BROKER_URL = "redis://127.0.0.1:6667"
    CELERY_BACKEND_URL = "db+postgresql://postgres:postgres@localhost:6666/postgres"
    ASYNC_THRESHOLD = 0.1


class ProdConfig(BaseConfig):
    """
    Config used in production
    """

    SQLALCHEMY_DATABASE_URI = os.environ.get("DATABASE_URL")
    CELERY_BACKEND_URL = os.environ.get("CELERY_BACKEND_URL")
    CELERY_BROKER_URL = os.environ.get("CELERY_BROKER_URL")
    SECRET_KEY = os.environ.get("SECRET_KEY")
    JWT_ALGORITHM = "HS512"
    ASYNC_THRESHOLD = 1
    task_serializer = "json"
    result_serializer = "json"
    task_track_started = True


config_map = {
    "test": TestConfig,
    "debug": BaseConfig,
    "prod": ProdConfig,
}
