"""remove taskmeta exists constraints

Revision ID: 6bdef8ca4ed9
Revises: c886870b0427
Create Date: 2020-12-06 08:47:51.045072

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = "6bdef8ca4ed9"
down_revision = "c886870b0427"
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.alter_column(
        "tasks", "task_meta_id", existing_type=sa.VARCHAR(length=155), nullable=True
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.alter_column(
        "tasks", "task_meta_id", existing_type=sa.VARCHAR(length=155), nullable=False
    )
    # ### end Alembic commands ###
