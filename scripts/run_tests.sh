export environment='test'
docker-compose -f test.docker-compose.yml up -d --build --force-recreate
sleep 0.5
py.test --cov="task_api"
coverage-badge -fo docs/coverage.svg
docker-compose -f test.docker-compose.yml down
docker rmi $(docker images --filter "dangling=true" -q --no-trunc)
docker rm $(docker ps -aq)
docker volume rm $(docker volume ls -q)
export environment='debug'