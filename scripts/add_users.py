from task_api.app import create_app
from task_api.users.model import User

if __name__ == "__main__":
    app = create_app()
    with app.app_context():
        if User.query.count() == 0:
            User.add(username="admin", password="admin", admin=True)
            User.add(username="user_1", password="user_1")
            User.add(username="user_2", password="user_2")
            print("added users")
